#!/usr/bin/python3

import argparse
from balancer import app, db

DEFAULT_PORT = 8000


def _parse_host(namespace):
    host = namespace.host
    port = namespace.port
    if not host:
        if ':' in namespace.address:
            host, port = namespace.address.split(':', 1)
        else:
            host = namespace.address
    port = int(port) if port else DEFAULT_PORT
    return host, port


def rundev(namespace):
    """Run developer server"""
    from werkzeug.serving import run_simple
    host, port = _parse_host(namespace)
    run_simple(application=app, hostname=host, port=port, use_reloader=True)


def runfcgi(namespace):
    """Run FastCGI server"""
    import os
    import sys
    from flup.server.fcgi import WSGIServer
    if namespace.daemonize:
        # daemonization code from django: become_daemon function
        # function parameters
        our_home_dir='.'
        out_log='/dev/null'
        err_log='/dev/null'
        umask=0o022

        # First fork
        try:
            if os.fork() > 0:
                sys.exit(0)     # kill off parent
        except OSError as e:
            sys.stderr.write("fork #1 failed: (%d) %s\n" % (e.errno, e.strerror))
            sys.exit(1)
        os.setsid()
        os.chdir(our_home_dir)
        os.umask(umask)

        # Second fork
        try:
            if os.fork() > 0:
                os._exit(0)
        except OSError as e:
            sys.stderr.write("fork #2 failed: (%d) %s\n" % (e.errno, e.strerror))
            os._exit(1)

        si = open('/dev/null', 'r')
        so = open(out_log, 'a+', 0)
        se = open(err_log, 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())
        # Set custom file descriptors so that they get proper buffering.
        sys.stdout, sys.stderr = so, se
    if namespace.pidfile:
        with open(namespace.pidfile, 'w') as fp:
            fp.write('{pid:d}\n'.format(pid=os.getpid()))
    WSGIServer(app, bindAddress=_parse_host(namespace)).run()


def updb(namespace):
    """Drop and recreate all tables in database."""
    import balancer.models
    import balancer.auth.models
    db.drop_all()
    db.create_all()


def filldb(namespace):
    """Try to fill database with some test data"""
    import balancer.test_data


def shell(namespace):
    """Return control to user with activated project environment"""
    import code
    code.interact(local=globals())

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Balancer management tool')

    subs = parser.add_subparsers()

    _run = subs.add_parser('run', help=rundev.__doc__)
    _run.add_argument('--host', default=None, required=False)
    _run.add_argument('--port', default=8000, type=int, required=False)
    _run.add_argument('address', default='localhost', nargs='?')
    _run.set_defaults(action_func=rundev)

    _updb = subs.add_parser('updb', help=updb.__doc__)
    _updb.set_defaults(action_func=updb)

    _fill = subs.add_parser('filldb', help=filldb.__doc__)
    _fill.set_defaults(action_func=filldb)

    _sh = subs.add_parser('shell', help=shell.__doc__)
    _sh.set_defaults(action_func=shell)

    args = parser.parse_args()
    args.action_func(args)