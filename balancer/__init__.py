import logging
from logging.handlers import RotatingFileHandler

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object('balancer.config.Debug')
if app.debug:
    log_level = 0
else:
    log_level = logging.getLogger(__name__).level
app.logger.setLevel(log_level)
rotate_file_handler = RotatingFileHandler(app.config['LOGFILE'], maxBytes=1024 * 100, backupCount=3)
rotate_file_handler.setLevel(log_level)
app.logger.addHandler(rotate_file_handler)

db = SQLAlchemy(app)

# blueprints
from .auth import Auth
app.register_blueprint(Auth)


# register views
import balancer.views.views

from balancer.models import ModelsJSEncoder
app.json_encoder = ModelsJSEncoder