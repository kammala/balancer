import os


class Config:
    DEBUG = False
    SECRET_KEY = 'devkey'
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

    SQLALCHEMY_DATABASE_URI = 'sqlite://:memory:'
    SQLALCHEMY_ECHO = False

    LOGIN_DEFAULT_NEXT_ENDPOINT = 'index'

    LOGFILE = os.path.abspath('./logfile')


class Debug(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://kammala:1@localhost/balancer'