var app = angular.module('profile', ['ui.bootstrap']);

app.controller('profile_ctrl', function ($scope, $http) {
    "use strict";
    $scope.adding = false;
    $scope.name = '';
    $scope.errors = null;

    $scope.owned_collapsed = true;
    $scope.full_collapsed = true;
    $scope.read_write_collapsed = true;
    $scope.read_only_collapsed = true;

    $http.get('/api/accounting/').success(function (data) {
        $scope.lists = angular.forEach(data, function (d) { angular.extend(d, {edit: false, errors: null});} );
    });

    $scope.add = function () {
        $scope.adding = true;
    };
    $scope.edit = function (list) {
        list.old = angular.copy(list);
        list.edit = true;
    };
    $scope.commit = function (list) {
        $http.put(list.api_url, {name: list.name}).success(function (){
            list.edit = false;
            list.old = null;
            list.errors = null;
        }).error(function(data, status){
            if (status === 400)
                list.errors = data;
        });
    };
    $scope.rollback = function (list) {
        angular.extend(list, list.old);
        list.old = null;
        list.errors = null;
        console.log(list.edit);
    };

    $scope.add_list = function () {
        var form_data = angular.element('form[name=adding_form]');
        $http.post(API_URL, {name: form_data.find('[name=name]').val()}).success(function (data) {
            angular.extend(data, {edit: false, errors: null});
            $scope.lists.push(data);
            $scope.adding = false;
            $scope.name = '';
        }).error(function(data, status) {
            if (status === 400)
                $scope.errors = data;
        });
    };

    $scope.cancel_adding = function () {
        $scope.adding=false;
    };

    $scope.remove_list = function (list) {
        var need_to_delete = window.confirm('Really delete accounting list?');
        if (!need_to_delete)
            return;
        $http.delete(list.api_url).success(function () {
            $scope.lists.splice($scope.lists.indexOf(list), 1);
        }).error(function (data, status) {
            if (status === 400)
                $scope.errors = data;
        });
    };

});