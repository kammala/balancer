from datetime import datetime, date
from wtforms import validators, Form, IntegerField, DateField, BooleanField, StringField, DecimalField
from wtforms.validators import ValidationError

from balancer import db
from balancer.models import Schedule, Accounting


class ExistedAccountingsValidator:
    def __init__(self, message='No such accounting: {}'):
        self.message = message

    def __call__(self, form, field):
        # if not (int(field.data),) in db.session.query(Accounting).values(Accounting.list_id):
        if not db.session.query(Accounting).filter(Accounting.list_id==int(field.data)).count():
            raise ValidationError(self.message.format(field.data))


class OffBooleanField(BooleanField):
    def process_formdata(self, valuelist):
        value = False
        if valuelist:
            value = valuelist[0] or ''
            value = value.lower() in ('on', 'true', 'yes', 'y')
        super(OffBooleanField, self).process_data(value)


class DonationForm(Form):
    list_id = IntegerField(validators=[validators.required(), ExistedAccountingsValidator()])
    name = StringField(validators=[validators.required()])
    size = IntegerField(validators=[validators.required()])
    description = StringField(validators=[validators.optional()])
    date = DateField(validators=[validators.required()],
                     filters=[lambda x: datetime.fromtimestamp(float(x))])

    period = StringField(validators=[validators.regexp(Schedule._period_pattern)])
    until = DateField(validators=[validators.optional()],
                      filters=[lambda x: datetime.fromtimestamp(float(x)) if x else None])
    disabled = BooleanField(validators=[validators.optional()])
    disabling_reason = StringField(validators=[validators.optional()])


class DonationsGetForm(Form):
    list_id = IntegerField(validators=[validators.required(), ExistedAccountingsValidator()])
    show_disabled = OffBooleanField(validators=[validators.optional()], default=False)
    start = DecimalField(validators=[validators.optional()],
                         filters=[lambda x: datetime.fromtimestamp(float(x)).date() if x else None])
    stop = DecimalField(validators=[validators.optional()],
                        filters=[lambda x: datetime.fromtimestamp(float(x)).date() if x else None])
    limit = IntegerField(validators=[validators.NumberRange(min=1), validators.optional()])
    offset = IntegerField(validators=[validators.NumberRange(min=0), validators.optional()], default=0)
    include_empty_tag = OffBooleanField(validators=[validators.optional()], default=False)


class AccountingForm(Form):
    name = StringField(validators=[validators.required(), validators.regexp(r'^[A-Za-z][\w\-. ]{3,}$')])