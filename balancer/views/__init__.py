import collections
from functools import wraps
from flask import make_response, json, g, request

from balancer import app
from balancer.models import DoesNotExists


class APIError(Exception):
    def __init__(self, message, status_code=400):
        self.message = message
        self.status_code = status_code
        super(APIError, self).__init__(message)
        app.logger.debug('Error: <{cls}:{st}> {msg}'.format(cls=self.__class__.__name__, st=status_code, msg=message))


class Forbidden(APIError):
    def __init__(self, message):
        super(Forbidden, self).__init__(message, status_code=403)


class NotFound(APIError):
    def __init__(self, message):
        super(NotFound, self).__init__(message, status_code=404)


class IncorrectParameters(APIError):
    def __init__(self, error_dict):
        super(IncorrectParameters, self).__init__(json.dumps(error_dict), status_code=400)


class Unauthorized(APIError):
    def __init__(self):
        super(Unauthorized, self).__init__('Unauthorized access is denied', status_code=401)


@app.errorhandler(IncorrectParameters)
def catch_param_errors(error):
    resp = make_response(error.message, error.status_code, None)
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.errorhandler(APIError)
def catch_api_errors(error):
    resp = make_response(json.dumps({error.__class__.__name__: error.message}), error.status_code, None)
    resp.headers['Content-Type'] = 'application/json'
    return resp


# helpers decorators
def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if g.user is None:
            raise Unauthorized()
        return f(*args, **kwargs)
    return decorated


def convert_to_apierror(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except DoesNotExists as ex:
            raise NotFound(str(ex))
    return decorated


def returns_json(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        ret = f(*args, **kwargs)
        content, status_code = ret if isinstance(ret, collections.Iterable) else ret, 200
        response = make_response(json.dumps(content), status_code, None)
        response.headers['Content-Type'] = 'application/json'
        return response
    return decorated
