from flask import render_template

from . import *

from balancer.auth import login_required
from balancer.models import *

DEFAULT_LIMIT = 10


def template_error_handler(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except NotFound as error:
            return render_template('404.jinja2', error=error), 404
    return decorated


@app.route('/')
def index():
    return render_template('index.jinja2', title='Home')


@app.route('/profile/')
@login_required
def profile():
    return render_template('profile.jinja2')


@app.route('/accounting/<accounting_id>/')
@template_error_handler
@login_required
def accounting(accounting_id):
    try:
        acc = Accounting.get(list_id=accounting_id)
    except DoesNotExists as ex:
        raise NotFound(str(ex))
    if not g.user.has_access(acc):
        raise Forbidden('You have not enough permissions to see this page.')
    show_disabled = (request.args.get('show_disabled') or '')
    show_disabled = show_disabled.lower() in ('on', 'true')
    return render_template('accounting.jinja2',
                           start=request.args.get('start'), stop=request.args.get('stop'),
                           show_disabled=show_disabled, page=int(request.args.get('page', 1)),
                           tags=request.args.getlist('tags'),
                           per_page=int(request.args.get('per_page', DEFAULT_LIMIT)), accounting=acc,
                           moderate_mode=g.user.has_access(acc, AccessType().full),
                           edit_mode=g.user.has_access(acc, AccessType().write),
                           access_types=[AccessType().read, AccessType().write, AccessType().full],
                           structure=json.dumps(DonationView(src=Donation(size=0, name='', date=datetime.date.today(),
                                                                          accounting=acc, author=g.user))))


# fixme: another module
@app.route('/api/search_tag/')
@returns_json
@login_required
def search_tag():
    return Tag.search(request.args.get('starts'))


@app.route('/api/search_user/')
@returns_json
@login_required
def search_user():
    try:
        return User.search(request.args.get('starts'), request.args.get('limit'))
    except ValueError as e:
        raise IncorrectParameters(str(e))