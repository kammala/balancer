from . import *
from .donationapi import DonationController
from .accountingapi import AccountingController

DonationAPI = DonationController.as_view('donation_api')
app.add_url_rule('/api/donation/<int:donation_id>/',
                 view_func=DonationAPI,
                 methods=['DELETE', 'PUT'])
app.add_url_rule('/api/donation/',
                 view_func=DonationAPI,
                 methods=['GET', 'POST'])

AccountingAPI = AccountingController.as_view('accounting_api')
app.add_url_rule('/api/accounting/<int:accounting_id>/',
                 view_func=AccountingAPI,
                 methods=['GET', 'DELETE', 'PUT'])
app.add_url_rule('/api/accounting/',
                 view_func=AccountingAPI,
                 methods=['GET', 'POST'])
from .accountingapi.rpc import invite, disinvite

from .rendered_template_views import index, profile, accounting, search_tag, search_user