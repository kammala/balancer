from flask.views import MethodView

from . import *
from balancer.forms import DonationForm, DonationsGetForm
from balancer.models import Donation, Accounting, AccessType, Tag


class DonationController(MethodView):
    decorators = (login_required, convert_to_apierror, returns_json)

    def post(self):
        data = DonationForm(**request.json)
        if not data.validate():
            raise IncorrectParameters(data.errors)
        accounting = Accounting.get(list_id=data.list_id.data)
        if not g.user.has_access(accounting, AccessType().write):
            raise Forbidden('Write access is denied to {}'.format(g.user.username))
        donat = Donation(author=g.user,
                         accounting=accounting,
                         description=data.description.data,
                         name=data.name.data,
                         date=data.date.data,
                         size=data.size.data,
                         period=data.period.data,
                         until=data.until.data)
        if data.period.data and data.disabled.data:
            donat.schedule.disable(on=data.date.data, reason=data.disabling_reason.data, author=g.user)
        for tag in {x.lower() for x in request.json.get('tags')}:
            # simple validation
            if len(tag) > Tag.name.property.columns[0].type.length:
                raise IncorrectParameters({'tag': ['Too long tag name: '+tag]})
            try:
                donat._tags.append(Tag.get(name=tag))
            except DoesNotExists:
                donat._tags.append(Tag(name=tag))
        donat.save()
        return donat.view()

    def get(self):
        parsed_params = DonationsGetForm(formdata=request.args)
        if parsed_params.validate():
            if not g.user.has_access(Accounting.get(list_id=parsed_params.list_id.data)):
                raise Forbidden('Read access is denied to {}'.format(g.user.username))
            acc = Accounting.get(list_id=parsed_params.list_id.data)
            tags = request.args.getlist('tags')
            if parsed_params.include_empty_tag.data:
                tags.append(None)
            data = list(acc.range(start=parsed_params.start.data,
                                  stop=parsed_params.stop.data,
                                  tags=tags,
                                  show_disabled=parsed_params.show_disabled.data))
            count = len(data)
            offset = parsed_params.offset.data or 0
            limit = parsed_params.limit.data
            if limit:
                data = data[offset:offset+limit]
            return {'count': count, 'data': data}
        else:
            raise IncorrectParameters(parsed_params.errors)

    def delete(self, donation_id):
        donat = Donation.get(donation_id=donation_id)
        if not g.user.has_access(donat.accounting, AccessType().write):
            raise Forbidden('Write access is denied for {}'.format(g.user.username))
        donat.delete()
        return None

    def put(self, donation_id):
        donat = Donation.get(donation_id=donation_id)
        if not g.user.has_access(donat.accounting, AccessType().write):
            raise Forbidden('Write access is denied for {}'.format(g.user.username))
        # fixme: validation bug without formdata
        data = DonationForm(accounting_id=donat.accounting.list_id, **request.json)
        if not data.validate():
            raise IncorrectParameters(data.errors)
        if not donat.schedule:
            if not data.period.data:
                if data.disabled.data:
                    return {'disabled': ["Can't disable not scheduled donation."]}, 400
            else:
                donat.add_schedule(period=data.period.data, until=data.until.data)
        else:
            if not data.period.data:
                donat.schedule = None
            else:
                donat.schedule.period = data.period.data
                donat.schedule.until = data.until.data
                if data.disabled.data:
                    donat.schedule.disable(on=data.date.data, reason=data.disabling_reason.data, author=g.user)
        donat.name = data.name.data
        donat.description = data.description.data
        donat.size = data.size.data
        donat.date = data.date.data
        donat.author = g.user
        # tags
        tags = {x.lower() for x in request.json.get('tags')}
        for x in set(donat.tags) - tags:
            donat.tags.remove(x)
        for tag in tags - set(donat.tags):
            # simple validation
            if len(tag) > Tag.name.property.columns[0].type.length:
                raise IncorrectParameters({'tag': ['Too long tag name: '+tag]})
            try:
                donat._tags.append(Tag.get(name=tag))
            except DoesNotExists:
                donat._tags.append(Tag(name=tag))
        donat.save()
        return donat