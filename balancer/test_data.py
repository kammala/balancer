from .models import *

user1 = User(username='kammala', password='1')
user2 = User(username='anorka', password='2')

db.session.add(user1)
db.session.add(user2)
db.session.commit()

read = AccessType().read
write = AccessType().write
full = AccessType().full

# db.session.add(read)
# db.session.add(write)
# db.session.add(full)
# db.session.commit()

acc_list1 = Accounting(name='kammala_list1', owner=user1)
acc_list2 = Accounting(name='anorka_list1', owner=user2)

db.session.add(acc_list1)
db.session.add(acc_list2)
db.session.commit()

perm_user2_list1 = Permission(user=user2, accounting=acc_list1, access=full, author=user1)

l1_d1 = Donation(name='test1', size=40, date=datetime.date(2014, 1, 1), accounting=acc_list1, author=user1)
l1_d2 = Donation(name='test2', size=20, date=datetime.date(2014, 1, 20), accounting=acc_list1, author=user1)

db.session.add(perm_user2_list1)
db.session.add(l1_d1)
db.session.add(l1_d2)
db.session.commit()

sch1 = Schedule()
sch1.donation = l1_d1
sch1.period = '1months'
sch1.author = user1

db.session.add(sch1)
db.session.commit()

dsbld1_sch1 = Disabled(date=datetime.date(2014, 8, 1), reason='Отпуск')
dsbld1_sch1.schedule = sch1
dsbld1_sch1.author = user1

dsbld2_sch1 = Disabled(date=datetime.date(2014, 9, 1), reason='Другая причина')
dsbld2_sch1.schedule = sch1
dsbld2_sch1.author = user1

db.session.add(dsbld1_sch1)
db.session.commit()