__author__ = 'kammala'

from functools import wraps
import logging

from flask import Blueprint, request, redirect, flash, url_for, g, session

LOGGER = logging.getLogger(__name__)

from .models import User

__all__ = ['views', 'models', 'Auth', 'super_required']

Auth = Blueprint('Auth', __name__, template_folder='templates')
from . import views


@Auth.before_app_request
def prepare_user():
    username = None
    # try basic HTTP auth header
    if 'AUTHORIZATION' in request.headers:
        LOGGER.debug('Try basic http auth')
        import base64
        credentials = request.headers['AUTHORIZATION'].split(' ', 1)[1]
        decoded = base64.b64decode(credentials).decode('utf-8')
        username, password = decoded.split(':', 1)
        LOGGER.debug('user: "{}" pass: "{}"'.format(username, password))
        if not User.validate_password(username, password):
            username = None
    if not username:
        username = session.get('username', None)
    if not getattr(g, 'user', None) or g.user.username != username:
        g.user = User.get_first(username=username)
        if g.user:
            session['username'] = g.user.username


def super_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if g.user is None:
            flash('You should log into system in order to access '
                  '<a href="{0}">{0}</a>'.format(request.url), category='danger')
            return redirect(url_for('Auth.login', next=request.url))
        if not g.user.superuser:
            flash('Insufficient privilegies for access to {}'.format(request.url), category='warning')
            return redirect(url_for('Auth.login', next=request.url))
        return f(*args, **kwargs)
    return decorated


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if g.user is None:
            flash('You should log into system in order to access '
                  '<a href="{0}">{0}</a>'.format(request.url), category='danger')
            return redirect(url_for('Auth.login', next=request.url))
        return f(*args, **kwargs)
    return decorated
