import re
import datetime
import time
import heapq
import itertools
import functools
from dateutil.relativedelta import relativedelta

from flask import json

from . import db, app
from flask.helpers import url_for
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import backref

from .auth.models import User

SMALL_STRING_LENGHT = 64
LONG_STRING_LENGHT = 512

READ_ACCESS = 1
WRITE_ACCESS = 2
FULL_ACCESS = 3


# exceptions
class DoesNotExists(Exception):
    def __init__(self, *args, **kwargs):
        super(DoesNotExists, self).__init__(*args, **kwargs)


class AlreadyExists(Exception):
    def __init__(self, *args, **kwargs):
        super(AlreadyExists, self).__init__(*args, **kwargs)


class PeriodFormatError(ValueError):
    def __init__(self, *args, **kwargs):
        super(PeriodFormatError, self).__init__(*args, **kwargs)


class PermissionDenied(Exception):
    def __init__(self, *args, **kwargs):
        super(PermissionDenied, self).__init__(*args, **kwargs)


class StandardDAOMixin:
    @classmethod
    def get(cls, **kwargs):
        ret = db.session.query(cls).filter_by(**kwargs).limit(2).all()
        if len(ret) > 1:
            msg = 'More than one {} instance. Params: {}'.format(cls.__qualname__, kwargs)
        elif len(ret) == 0:
            msg = 'There is no such {} instance. Params: {}'.format(cls.__qualname__, kwargs)
        else:
            return ret[0]
        raise DoesNotExists(msg)

    SEARCH_LIMIT = 10

    @classmethod
    def search(cls, starts):
        if not hasattr(cls, 'name'):
            raise NotImplemented()
        return db.session.query(cls).filter(cls.name.startswith(starts)).order_by(cls.name)[:cls.SEARCH_LIMIT]

    def __init__(self):
        pass

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError:
            raise AlreadyExists('Already exists {} with such keys'.format(self.__class__.__name__))

    def as_json(self):
        raise NotImplemented()

tags_donations_m2m = db.Table('tags_donations_m2m', db.Model.metadata,
                              db.Column('tag_id', db.Integer, db.ForeignKey('tags.tag_id'), nullable=False),
                              db.Column('donation_id', db.Integer, db.ForeignKey('donations.donation_id'), nullable=False),
                              db.PrimaryKeyConstraint('tag_id', 'donation_id', name='pk__tags_donation_m2m'))


class Tag(db.Model, StandardDAOMixin):
    __tablename__ = 'tags'

    tag_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(SMALL_STRING_LENGHT), nullable=False, unique=True)

    def __init__(self, name):
        super(Tag, self).__init__()
        self.name = name.lower()

    def __repr__(self):
        return '<{}> {}({})'.format(self.__class__.__name__, self.name, self.tag_id)

    def as_json(self):
        return dict(id=self.tag_id, name=self.name)


class Accounting(db.Model, StandardDAOMixin):
    __tablename__ = 'accountings'

    list_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(SMALL_STRING_LENGHT), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)

    owner = db.relationship('User', backref=backref('created', cascade='all, delete, delete-orphan'))

    __table_args__ = (
        db.UniqueConstraint('name', 'owner_id', name='ak__accounting_uniq_name_and_owner'),
    )

    def __repr__(self):
        return '<{_class}> {name}'.format(_class=self.__class__.__name__, name=self.name)

    def __iter__(self):
        return heapq.merge(*self.raw_donations)

    def range(self, *, start=None, stop=None, show_disabled=False, tags=None, desc=True):
        """range(start=None, stop=datetime.date.today(), show_disabled=False) -> iterator.

Return all donations in interval [start, stop].
By default, start is the earlest donation in list, stop -- today.
:param: show_disabled indicates whether disabled items should be returned."""
        # drop all until start
        stop = stop or datetime.date.today()
        it = self
        it = itertools.dropwhile(lambda x: x.date < start, it) if start else it
        it = itertools.takewhile(lambda x: x.date <= stop, it)
        if not show_disabled:
            it = filter(lambda x: not x.disabled, it)
        if tags:
            tags_set = set(tags)
            filtr = lambda x: not tags_set.isdisjoint(x.tags)
            if None in tags:
                filtr = lambda x: not x.tags or not tags_set.isdisjoint(x.tags)
            it = filter(filtr, it)
        if desc:
            it = reversed(list(it))
        yield from it

    def __init__(self, owner, name):
        super(Accounting, self).__init__()
        self.name = re.sub(r'\s+', ' ', name.strip())
        self.owner = owner

    def view(self, access_type=None):
        return AccountingView(self, access_type)


class AccountingView:
    def __init__(self, source, access_type):
        self.src = source
        self.access_type = access_type

    def __getattr__(self, attr):
            return self.src.__getattribute__(attr)


class AccessType(db.Model):
    __tablename__ = 'access_types'

    access_type_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(SMALL_STRING_LENGHT), unique=True, nullable=False)

    __read = None
    __write = None
    __full = None

    @property
    def read(self):
        if not self.__read:
            self.__read = db.session.query(self.__class__).filter_by(name='read').first()
            if not self.__read:
                self.__read = self.__class__(name='read', access_type_id=READ_ACCESS)
                db.session.add(self.__read)
                db.session.commit()
        return self.__read

    @property
    def write(self):
        if not self.__write:
            self.__write = db.session.query(self.__class__).filter_by(name='write').first()
            if not self.__write:
                self.__write = self.__class__(name='write', access_type_id=WRITE_ACCESS)
                db.session.add(self.__write)
                db.session.commit()
        return self.__write

    @property
    def full(self):
        if not self.__full:
            self.__full = db.session.query(self.__class__).filter_by(name='full').first()
            if not self.__full:
                self.__full = self.__class__(name='full', access_type_id=FULL_ACCESS)
                db.session.add(self.__full)
                db.session.commit()
        return self.__full

    def __repr__(self):
        return '<{_class}> {name}({value})'.format(_class=self.__class__.__name__,
                                                   name=self.name, value=self.access_type_id)

    def __lt__(self, other):
        if isinstance(other, (int,)):
            other = self.__class__.get(access_type_id=other)
        if isinstance(other, self.__class__):
            return self.access_type_id < other.access_type_id

    def __eq__(self, other):
        if self is other:
            return True
        if isinstance(other, (int,)):
            other = self.__class__.get(access_type_id=other)
        if isinstance(other, self.__class__):
            return self.access_type_id == other.access_type_id


class Permission(db.Model, StandardDAOMixin):
    __tablename__ = 'permissions'

    permission_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    list_id = db.Column(db.Integer, db.ForeignKey('accountings.list_id'), nullable=False)
    access_type_id = db.Column(db.Integer, db.ForeignKey('access_types.access_type_id'), nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)

    author = db.relationship('User', foreign_keys=[author_id])
    access = db.relationship('AccessType')
    user = db.relationship('User', foreign_keys=[user_id], backref='all_shared')
    accounting = db.relationship('Accounting',
                                 backref=backref('permissions', cascade='all, delete, delete-orphan'))

    __table_args__ = (
        db.UniqueConstraint('user_id', 'list_id', name='ak__permissions'),
    )

    def __repr__(self):
        return '<{_class}> {list}:{user}:{access}'.format(_class=self.__class__.__name__, list=self.accounting.name,
                                                          user=self.user.username, access=self.access.name)
    
    def as_json(self):
        return dict(accounting=self.list_id,
                    author=self.author_id,
                    user=self.user_id,
                    access=self.access_type_id,

                    d_accounting=self.accounting.name,
                    d_author=self.author.username,
                    d_user=self.user.username,
                    d_access=self.access.name)


class Donation(db.Model, StandardDAOMixin):
    __tablename__ = 'donations'

    donation_id = db.Column(db.Integer, primary_key=True)
    size = db.Column(db.Float, nullable=False)
    date = db.Column(db.Date, nullable=False)
    name = db.Column(db.String(SMALL_STRING_LENGHT), nullable=False)
    description = db.Column(db.Text, nullable=True)
    list_id = db.Column(db.Integer, db.ForeignKey('accountings.list_id'), nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)

    accounting = db.relationship('Accounting', backref=backref('raw_donations', cascade='all, delete, delete-orphan'))
    author = db.relationship('User')

    _tags = db.relationship('Tag', secondary=tags_donations_m2m)
    tags = association_proxy('_tags', 'name')

    __table_args__ = (
        db.UniqueConstraint('name', 'date', 'list_id', name='ak__donations'),
    )

    def __repr__(self):
        return '<{_class}> [{date}] {name} ({size})'.format(_class=self.__class__.__name__,
                                                            name=self.name, date=self.date, size=self.size)

    def view(self):
        return DonationView(self)

    def __iter__(self):
        yield from self.schedule or [self.view()]

    def __init__(self, **kwargs):
        super(Donation, self).__init__()
        required = ('size', 'name', 'date', 'accounting', 'author')
        optional = ('description', 'period', 'until')

        if set(required) - kwargs.keys():
            raise ValueError('{}: required params [{}] are absent.'.format(
                self.__class__.__qualname__, ','.join(set(required) - kwargs.keys())))
        for req in required:
            setattr(self, req, kwargs.get(req))

        self.description = kwargs.get('description', None)
        if 'period' in kwargs and kwargs.get('period'):
            self.add_schedule(period=kwargs.get('period'), until=kwargs.get('until', None) or None,
                              author=self.author, commit=False)
        else:
            self.schedule = None

    def add_schedule(self, period, until=None, author=None, commit=False):
        if not author:
            author = self.author
        self.schedule = Schedule(donation=self, author=author, until=until)
        self.schedule.period = period
        if commit:
            db.session.add(self.schedule)
            db.session.commit()

    def delete_schedule(self):
        db.session.delete(self.schedule)
        db.commit()


class DonationView:
    def __init__(self, src, date=None, disabled=False, disabling_reason=None):
        self.src = src
        self.date = date or src.date
        self.disabled = disabled
        self.disabling_reason = disabling_reason

    def __getattr__(self, attr):
        return self.src.__getattribute__(attr)

    def __repr__(self):
        return '<{_class}> [{date}] {name} ({size})'.format(_class=self.__class__.__name__, name=self.name,
                                                            date=self.date, size=self.size)

    def __lt__(self, other):
        return self.date < other.date

    def __gt__(self, other):
        return self.date > other.date


class Schedule(db.Model):
    __tablename__ = 'schedules'

    schedule_id = db.Column(db.Integer, primary_key=True)
    donation_id = db.Column(db.Integer, db.ForeignKey('donations.donation_id'), nullable=False, unique=True)
    __period = db.Column('period', db.String(SMALL_STRING_LENGHT), nullable=False)
    __period_delta = None
    author_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    until = db.Column(db.Date, nullable=True)

    author = db.relationship('User')
    donation = db.relationship('Donation',
                               backref=backref('schedule', uselist=False, cascade='all, delete, delete-orphan'))

    _period_pattern = re.compile(
        r'^\s*((?P<years>\d+)\s*(y|yy|yyyy|years))?\s*((?P<months>\d+)\s*(m|mon|months))?\s*((?P<weeks>\d+)\s*(w|weeks))?\s*((?P<days>\d+)\s*(d|dd|days))?\s*$'
    )

    @property
    def period(self):
        if self.__period_delta is None:
            m = self._period_pattern.match(self.__period)
            self.__period_delta = relativedelta(
                days=int(m.groupdict()['days'] or 0),
                months=int(m.groupdict()['months'] or 0),
                years=int(m.groupdict()['years'] or 0)
            )
        return self.__period_delta

    @period.setter
    def period(self, value):
        matched = self._period_pattern.match(value)
        if matched and any(matched.groupdict().values()):
            self.__period = ''
            years = 0
            if matched.groupdict()['years']:
                self.__period += matched.groupdict()['years']+'y'
                years = int(matched.groupdict()['years'])
            months = 0
            if matched.groupdict()['months']:
                self.__period += matched.groupdict()['months']+'m'
                months = int(matched.groupdict()['months'])
            days = 0
            if matched.groupdict()['weeks']:
                days += int(matched.groupdict()['weeks']) * 7
            if matched.groupdict()['days']:
                days += int(matched.groupdict()['days'])
            if days:
                self.__period += str(days)+'d'
            self.__period_delta = relativedelta(days=days, months=months, years=years)
        else:
            raise PeriodFormatError('Incorrect period format')

    def __iter__(self):
        dt = self.donation.date
        disabled_it = iter(self.disabled)
        disabled = next(disabled_it, None)
        while True:
            if self.until is not None and dt > self.until:
                raise StopIteration
            if disabled and dt > disabled.date:
                disabled = next(disabled_it, None)
            clone = self.donation.view()
            clone.date = dt
            clone.disabled = disabled.date == dt if disabled else False
            clone.disabling_reason = None if not disabled else disabled.reason
            yield clone
            dt += self.period

    def disable(self, on, reason, author=None, commit=False):
        dis = db.session.query(Disabled).filter_by(schedule=self, date=on).first()
        if not dis:
            dis = Disabled(date=on, reason=reason, author=author, schedule=self)
        else:
            dis.reason = reason
            dis.author = author
        if commit:
            db.session.add(dis)
            db.session.commit()

    def __repr__(self):
        return '<{_class}> {donation}::{period}'.format(_class=self.__class__.__name__,
                                                        donation=self.donation.name, period=self.__period)


class Disabled(db.Model, StandardDAOMixin):
    __tablename__ = 'disabled'

    schedule_id = db.Column(db.Integer, db.ForeignKey('schedules.schedule_id'), nullable=False)
    date = db.Column(db.Date, nullable=False)
    reason = db.Column(db.String(LONG_STRING_LENGHT))
    author_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)

    author = db.relationship('User')
    schedule = db.relationship('Schedule', backref=backref('disabled',
                                                           order_by='asc(Disabled.date)',
                                                           cascade='all, delete, delete-orphan'))

    __table_args__ = (
        db.PrimaryKeyConstraint('schedule_id', 'date', name='pk__disabled_schedules'),
    )

    def __repr__(self):
        return '<{_class}> {sch} on {date}'.format(_class=self.__class__.__name__,
                                                   date=self.date, sch=self.schedule.donation.name)


# extra user fields
def _shared(user, access_type_id):
    more = db.session.query(Accounting).\
        join(Permission).filter(Permission.user == user, Permission.access_type_id>access_type_id)
    return db.session.query(Accounting).\
        join(Permission).\
        filter(Permission.user == user, Permission.access_type_id == access_type_id).\
        filter(~more.exists())
# warning: hardcoded ids
# needed to recreate database
User.moderable_shared = property(functools.partial(_shared, access_type_id=FULL_ACCESS))
User.writeonly_shared = property(functools.partial(_shared, access_type_id=WRITE_ACCESS))
User.readonly_shared = property(functools.partial(_shared, access_type_id=READ_ACCESS))


def _has_access(self, accounting, access=None):
    if accounting.owner == self:
        # owner has all rights on accounting
        return True
    check = db.session.query(Permission).filter(Permission.user == self, Permission.accounting == accounting)
    if access:
        check = check.filter(Permission.access_type_id >= access.access_type_id)
    return db.session.query(check.exists()).first()[0]
User.has_access = _has_access


_default_access = object()
def _manage_permissions(self, accounting, user, user_access=_default_access, force_update=False):
    if user_access is _default_access:
        user_access = AccessType().read
    if self.has_access(accounting, user_access):
        try:
            perm = Permission.get(accounting=accounting, user=user)
            if user_access is None:
                perm.delete()
                return
            if perm.access < user_access or force_update:
                perm.access = user_access
                perm.author = self
            else:
                # raise some exception instead?
                return
        except DoesNotExists:
            if user_access:
                perm = Permission(accounting=accounting, author=self, user=user, access=user_access)
            else:
                return
        perm.save()
    else:
        raise PermissionDenied('{} access denied to "{}"'.format(user_access.name.capitalize(), self.username))
User.manage_permissions = _manage_permissions


def _search(cls, starts, limit=None):
    # fixme: magic number
    SEARCH_LIMIT = 100
    limit = limit if limit and limit > 0 else SEARCH_LIMIT
    try:
        limit = int(limit)
    except:
        raise ValueError("Invalid input for limit")
    limit = min(limit, SEARCH_LIMIT)

    if not starts:
        raise ValueError("Empty 'starts' parameter")
    return db.session.query(cls).filter(cls.username.startswith(starts)).order_by(cls.username)[:limit]
User.search = classmethod(_search)


def _as_json(self):
    return dict(name=self.username, id=self.user_id)
User.as_json = _as_json


class ModelsJSEncoder(json.JSONEncoder):
    def default(self, o):
        # datetime, timedelta
        if isinstance(o, (datetime.datetime, datetime.date)):
            return time.mktime(o.timetuple())
        elif isinstance(o, relativedelta):
            str_period = ''
            if o.years:
                str_period += '{} years '.format(o.years)
            if o.months:
                str_period += '{} months '.format(o.months)
            if o.days:
                str_period += '{} days'.format(o.days)
            return str_period.strip()
        # models
        elif isinstance(o, (DonationView, Donation)):
            if isinstance(o, Donation):
                o = o.view()
            ret = dict(date=o.date, name=o.name, description=o.description, id=o.donation_id, size=o.size,
                       disabled=o.disabled, disabling_reason=o.disabling_reason, tags=list(o.tags))
            ret.update(period=o.schedule.period if o.schedule else None, until=o.schedule.until if o.schedule else None)
            return ret
        # accountings 'owned'
        elif isinstance(o, (Accounting, AccountingView)):
            if isinstance(o, Accounting):
                # fixme: may any other type be possible?
                o = o.view(access_type='owned')
            return dict(name=o.name, id=o.list_id, type=o.access_type,
                        api_url=url_for('accounting_api', accounting_id=o.list_id),
                        view_url=url_for('accounting', accounting_id=o.list_id))
        elif isinstance(o, StandardDAOMixin) or hasattr(o, 'as_json'):
            return o.as_json()
        return super(ModelsJSEncoder, self).default(o)